<?php

require_once "animal.php";
require_once "frog.php";
require_once "ape.php";

$sheep = new Animal("shaun");

echo "Nama hewan: " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki: " . $sheep->legs . "<br>"; // 2
echo "Berdarah dingin: " . $sheep->cold_blooded . "<br>";// false
echo "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Nama hewan: " . $sungokong->name . "<br>"; 
echo "Jumlah kaki: " . $sungokong->legs . "<br>"; 
echo "Berdarah dingin: " . $sungokong->cold_blooded . "<br>";
$sungokong->yell(); // "Auooo"
echo "<br><br>";

$kodok = new Frog("buduk");

echo "Nama hewan: " . $kodok->name . "<br>"; 
echo "Jumlah kaki: " . $kodok->legs . "<br>"; 
echo "Berdarah dingin: " . $kodok->cold_blooded . "<br>";
$kodok->jump(); // "hop hop"




?>